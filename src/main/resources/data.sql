INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (0, 'T-Shirt Maroc HOME', 'T-Shirt Puma Officiel Rouge et Vert de l''équipe du Maroc', 'https://images.puma.com/image/upload/f_auto,q_auto,b_rgb:fafafa,w_600,h_600/global/765809/01/fnd/DFA/fmt/png/Morocco-Home-22/23-Replica-Jersey-Youth', 950.0);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (1, 'T-Shirt Maroc AWAY', 'T-Shirt Puma Officiel Blanc de l''équipe du Maroc.', 'https://images.puma.com/image/upload/f_auto,q_auto,b_rgb:fafafa,w_600,h_600/global/765812/02/fnd/DFA/fmt/png/Morocco-Away-22/23-Replica-Jersey-Men', 950.0);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (2, 'Jacket Maroc', 'Jacket Puma officielle d''avant match de l''équipe du Maroc', 'https://fanatics.frgimages.com/morocco-national-team/mens-puma-green-morocco-national-team-pre-match-raglan-full-zip-training-jacket_pi4919000_altimages_ff_4919172-006670e3ebdc6891ae33alt2_full.jpg?_hv=2&w=900', 1550.0);

INSERT INTO PRODUCT (ID , TITRE ,DESCRIPTION ,IMAGE ,PRIX ) VALUES (3, 'Drapeau Maroc', 'Drapeau du Maroc pour supporters', 'https://www.macapflag.com/1857982-medium_default/drapeau-maroc-avec-hampe-officiel.jpg', 50.0);
